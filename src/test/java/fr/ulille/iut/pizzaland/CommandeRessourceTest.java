package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.resources.CommandeResource;

public class CommandeRessourceTest extends JerseyTest{

	private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());
	private CommandeDao commandeDao;
	
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		
		return new ApiV1();
	}
	
	@Before
	public void setEnvUp() {
		commandeDao = BDDFactory.buildDao(CommandeDao.class);
		commandeDao.createCommandeTable();
	}
	
	@After
	public void tearEnvDown() throws Exception {
		commandeDao.dropTableCommande();
	}
	
	@Test
	public void testGetEmptyList() {
		Response response = target("/commandes").request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {});
		
		assertEquals(0, commandes.size());
	}
	
	@Test
	public void testGetExistingCommande() {
		Commande commande = new Commande();
		commande.setNom("Xavier");
		
		long id = commandeDao.insert(commande.getNom());
		commande.setId(id);
		
		Response response = target("/commandes/"+id).request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);
		
	}
	
	@Test
	public void testGetNotExistingCommande() {
		Response response = target("commandes/4224").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateCommande() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setName("Xavier");
		
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		
		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);
		
		assertEquals(target("/commandes/"+returnedEntity.getId()).getUri(), response.getLocation());
	}
	
	@Test
	public void testCreateSameCommande() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setName("Xavier");
		commandeDao.insert(commandeCreateDto.getName());
		
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		
		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateCommandeWithoutName() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		
		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setNom("Xavier");
		long id = commandeDao.insert(commande.getNom());
		commande.setId(id);
		
		Response response = target("/commandes/"+ id).request().delete();
		
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
		
		Commande result = commandeDao.findById(id);
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes/125").request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testGetCommandeName() {
		Commande commande = new Commande();
		commande.setNom("Xavier");
		long id = commandeDao.insert(commande.getNom());
		
		Response response = target("commandes/"+id+"/name").request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		assertEquals("Xavier", response.readEntity(String.class));
	}
	
	@Test
	public void testGetNotExistingCommandeName() {
		Response response = target("commandes/4242/name").request().get();
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
}
