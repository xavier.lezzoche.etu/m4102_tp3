package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class PizzaRessourceTest extends JerseyTest{

	private static final Logger LOGGER = Logger.getLogger(PizzaRessourceTest.class.getName());
	private PizzaDao pizzaDao;
	
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		
		return new ApiV1();
	}
	
	@Before
	public void setEnvUp() {
		pizzaDao = BDDFactory.buildDao(PizzaDao.class);
		pizzaDao.createPizzaTable();
	}
	
	@After
	public void tearEnvDown() throws Exception {
		pizzaDao.dropTablePizzaAndAssociation();
	}
	
	@Test
	public void testGetEmptyList() {
		Response response = target("/pizzas").request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {});
		
		assertEquals(0, pizzas.size());
	}
	
	@Test
	public void testGetExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Orientale");
		
		long id = pizzaDao.insert(pizza.getName());
		pizza.setId(id);
		
		Response response = target("/pizzas/"+id).request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}
	
	@Test
	public void testGetNotExistingPizza() {
		Response response = target("pizzas/125").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreatePizza() {
		
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Orientale");
		
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		
		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);
		
		assertEquals(target("/pizzas/"+returnedEntity.getId()).getUri(), response.getLocation());
	}
	
	@Test
	public void testCreateSamePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Orientale");
		pizzaDao.insert(pizzaCreateDto.getName());
		
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		
		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Orientale");
		long id = pizzaDao.insert(pizza.getName());
		pizza.setId(id);
		
		Response response = target ("/pizzas/" + id).request().delete();
		
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
		
		Pizza result = pizzaDao.findById(id);
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas/125").request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("Orientale");
		long id = pizzaDao.insert(pizza.getName());
		
		Response response = target("pizzas/"+id+"/name").request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		assertEquals("Orientale", response.readEntity(String.class));
	}
	
	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("pizzas/125/name").request().get();
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
}
