package fr.ulille.iut.pizzaland.dto;

public class CommandeCreateDto {
	private String name;
	
	public CommandeCreateDto() {}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
