package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
	@SqlUpdate("Create table if not exists pizzas (id integer primary key, name varchar unique not null)")
	void createPizzaTable();
	
	@SqlUpdate("Create table if not exists pizzaIngredientAssociation (idPizza integer, idIngredient integer, constraint pk_idid primary key(idPizza, idIngredient), constraint fk_idPizza foreign key(idPizza) references pizzas(id), constraint fk_idIngredient foreign key(idIngredient) references ingredients(id)")
	void createPizzaAssociationTable();
	
	@Transaction
	default void createPizzaAndAssociationTable() {
		createPizzaTable();
		createPizzaAssociationTable();
	}
	
	@SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);
	
	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();
	
	@SqlQuery("Select * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);
	
	@SqlQuery("Select * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);
	
	@SqlUpdate("Drop table if exists pizzas")
	void dropTablePizza();
	
	@SqlUpdate("Drop table if exists pizzaIngredientAssociation")
	void dropTablePizzaAssociation();
	
	default void dropTablePizzaAndAssociation() {
		dropTablePizza();
		dropTablePizzaAssociation();
	}
	
	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void remove(long id);

}
