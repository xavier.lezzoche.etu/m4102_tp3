package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {

	@SqlUpdate("Create table if not exists commandes(id integer primary key, name varchar unique not null)")
	void createCommandeTable();
	
	@SqlUpdate("Create table if not exists commandePizzaAssociation (idCommande integer, idPizza integer, constraint pk_idCidP primary key(idCommande, idPizza), constraint fk_idCommande foreign key(idCommande) references commandes(id), constraint fk_idPizza foreign key(idPizza) references pizzas(id)")
	void createCommandeAssociationTable();
	
	@Transaction
	default void createCommandeAndAssociationTable() {
		createCommandeTable();
		createCommandeAssociationTable();
	}
	
	@SqlUpdate("INSERT INTO commandes(name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);
	
	@SqlQuery("Select * FROM commandes")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();
	
	@SqlQuery("Select * FROM commandes WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findById(long id);
	
	@SqlQuery("SELECT * FROM commandes WHERE name = :name")
	@RegisterBeanMapper(Commande.class)
	Commande findByName(String name);
	
	@SqlUpdate("Drop table if exists commandes")
	void dropTableCommande();
	
	@SqlUpdate("Drop table if exists commandePizzaAssociation")
	void dropTableCommandeAssociation();
	
	default void dropTableCommandeAndAssociation() {
		dropTableCommande();
		dropTableCommandeAssociation();
	}
	
	@SqlUpdate("DELETE FROM commandes WHERE id = :id")
	void remove(long id);
}
